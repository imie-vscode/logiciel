const { app, BrowserWindow } = require("electron");
const path = require("path");
const isDev = require("electron-is-dev");

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    webPreferences: {
      enableRemoteModule: true,
      nodeIntegration: false,
      nodeIntegrationInWorker: false,
      webSecurity: true,
      // devTools: isDev ? true : false,
      preload: __dirname + "/preload.js",
      worldSafeExecuteJavaScript: true,
    },
    show: false,
  });
  mainWindow.once("ready-to-show", () => {
    mainWindow.show();
  });
  mainWindow.maximize(); // plein écran
  mainWindow.loadURL(isDev ? "http://localhost:3000" : `file://${path.join(__dirname, "../build/index.html")}`);
  // mainWindow.loadFile(`${path.join(__dirname, "/build/index.html")}`);
  mainWindow.on("closed", () => (mainWindow = null));
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
