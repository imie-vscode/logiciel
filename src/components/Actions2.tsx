import React from "react";
import { Button } from "primereact/button";
import { Link } from "react-router-dom";

const Actions2 = () => {
  return (
    <Link to="/user-id">
      <Button icon="pi pi-user" className="p-button-rounded p-button-info" />
    </Link>
  );
};
export default Actions2;
