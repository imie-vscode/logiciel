import React, { useState } from "react";
import { Button } from "primereact/button";
import { ConfirmDialog } from "primereact/confirmdialog";

const Actions: React.FC = () => {
  const [visible, setVisible] = useState(false);

  const accept = () => {};
  const reject = () => {};

  const Modal = () => {
    return (
      <ConfirmDialog
        visible={visible}
        onHide={() => setVisible(false)}
        message="voulez vraiment continuer?"
        icon="pi pi-exclamation-triangle"
        accept={accept}
        reject={reject}
      />
    );
  };
  return (
    <div className="header-buttons">
      {Modal()}
      <Button
        onClick={() => setVisible(true)}
        icon="pi pi-times"
        className="p-button-rounded p-button-danger p-button-text"
      />
    </div>
  );
};

export default Actions;
