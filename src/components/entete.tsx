import React from "react";
import { FormControl } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import Deconnexion from "./deconnexion";

const Entete = () => {
  return (
    <Card.Header as="h5">
      <div className="p-grid">
        <div className="p-col-3">
          {/* <button onClick={handleClick}>+</button> */}
          Tous les Utilisateurs(2)
        </div>
        <div className="p-col-5"></div>
        <div className="p-col-3">
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          {/* button submit a revoir */}
        </div>
        <div className="p-col-1">
          <Deconnexion></Deconnexion>
        </div>
      </div>
    </Card.Header>
  );
};
export default Entete;
