//import { Link } from "react-router-dom";
//import Card from 'react-bootstrap/Card';

import React from "react";
import { PanelMenu } from "primereact/panelmenu";
import "../index.css";

const LeftMenu = () => {
  const items = [
    {
      label: "Utilisateurs",
      items: [
        {
          label: "Tous les utilisateurs",
          url: "#/list-user",
        },
      ],
    },
    {
      label: "Projets",
      items: [
        {
          label: "Liste des projets",
          url: "#/project",
        },
      ],
    },
    {
      label: "Employés",
      items: [
        {
          label: "Liste des employés",
          url: "#/employe-list",
        },
        {
          label: "Gestion des rôles",
          url: "#/role",
        },
        {
          label: "Ajouter un employé",
          url: "#/new-employe",
        },
      ],
    },
    {
      label: "Abonnement",
      items: [
        {
          label: "Liste des abonnements",
          url: "#/",
        },
      ],
    },
  ];
  return (
    <div style={{ marginTop: "5%" }}>
      <PanelMenu className="menu" model={items} style={{ width: "20rem" }} />
    </div>
  );
};

export default LeftMenu;
