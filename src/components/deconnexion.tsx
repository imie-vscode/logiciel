import React, { useState } from "react";
import { Link } from "react-router-dom";
import { NavDropdown } from "react-bootstrap";
import { withCookies, useCookies } from "react-cookie";

const Deconnexion = () => {
  const [cookie, setCookie, removeCookie] = useCookies(["user"]);

  function handleSetCookie() {
    setCookie("user", "obydul", { path: "/" });
  }

  function handleRemoveCookie() {
    removeCookie("user");
  }

  return (
    <NavDropdown title="" id="nav-dropdown">
      <NavDropdown.Item eventKey="4.1">Nom complet</NavDropdown.Item>
      <NavDropdown.Item eventKey="4.2">adresse mail </NavDropdown.Item>
      <NavDropdown.Divider />
      <Link to="/sign-in">
        <button type="submit" id="white" onClick={handleRemoveCookie}>
          <h6 id="white">se deconnecter</h6>
        </button>
      </Link>
    </NavDropdown>
  );
};

export default Deconnexion;
