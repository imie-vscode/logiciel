import { Row } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import LeftMenu from "../components/leftmenu";
import "primeflex/primeflex.css";
import Actions from "../components/actions";
import Deconnexion from "../components/deconnexion";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { Link } from "react-router-dom";

const fakeData = [
  {
    Rôle: "Administrateur",
  },
  {
    Rôle: "Utilisateur",
  },
  {
    Rôle: "Comptable",
  },
  {
    Rôle: "Commercial",
  },
];

const GestionRole = () => {
  // function handleClick() {
  //     alert('Le lien a été cliqué.');
  //   }
  return (
    <div className="p-grid">
      <Card id="menu">
        <div className="p-col-3">
          <div className="p-col-2" id="menu">
            <Link to="/list-user">
              <i className="pi pi-arrow-left" style={{ fontSize: "2em" }}></i>
            </Link>
          </div>
          <LeftMenu />
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </Card>
      <div className="p-col-9">
        <Row>
          <div className="p-col-11">
            <h3>{`Rôles (${fakeData.length})`}</h3>
          </div>
          <div className="p-col-1">
            <Deconnexion></Deconnexion>
          </div>{" "}
        </Row>
        <DataTable
          value={fakeData}
          tableStyle={{ borderRadius: 15 }}
          className="p-datatable-striped"
          selectionMode="single"
        >
          <Column
            field="Rôle"
            header="Rôle label"
            style={{ width: "80%" }}
            filter
            sortable
          />

          <Column style={{ width: "20%" }} body={() => <Actions />} />
        </DataTable>
      </div>
    </div>
  );
};
export default GestionRole;
