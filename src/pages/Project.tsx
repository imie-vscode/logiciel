import { Link } from "react-router-dom";
import { Container, Image } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { Row, Col } from "react-bootstrap";
import LeftMenu from "../components/leftmenu";
import Deconnexion from "../components/deconnexion";
import React from "react";
import { TabView, TabPanel } from "primereact/tabview";

const project = () => {
  return (
    <div className="p-grid">
      <Card id="menu">
        <div className="p-col-3" id="menu">
          <div className="p-col-2" id="menu">
            <Link to="/list-user">
              <i className="pi pi-arrow-left" style={{ fontSize: "2em" }}></i>
            </Link>
          </div>
          <LeftMenu />
        </div>
      </Card>
      <div className="p-col-9">
        <Row>
          <div className="p-col-11">
            <h3>{`Liste des projets`}</h3>
          </div>
          <div className="p-col-1">
            <Deconnexion></Deconnexion>
          </div>{" "}
        </Row>
        <div className="card">
          <TabView>
            <TabPanel header="Tous les projets">
              <Row>
                <Col xs={4}>
                  <Card>
                    <Card.Header>Nom_projet</Card.Header>
                    <Card.Body>
                      <Card.Title>Description</Card.Title>
                      <Card.Text>Détails...</Card.Text>
                    </Card.Body>
                    <Card.Footer className="text-muted">
                      <Container>
                        <Row>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Container>
                    </Card.Footer>
                  </Card>
                </Col>
                <Col xs={4}>
                  <Card>
                    <Card.Header>Nom_projet</Card.Header>
                    <Card.Body>
                      <Card.Title>Description</Card.Title>
                      <Card.Text>Détails...</Card.Text>
                    </Card.Body>
                    <Card.Footer className="text-muted">
                      <Container>
                        <Row>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Container>
                    </Card.Footer>
                  </Card>
                </Col>
                <Col xs={4}>
                  <Card>
                    <Card.Header>Nom_projet</Card.Header>
                    <Card.Body>
                      <Card.Title>Description</Card.Title>
                      <Card.Text>Détails...</Card.Text>
                    </Card.Body>
                    <Card.Footer className="text-muted">
                      <Container>
                        <Row>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Container>
                    </Card.Footer>
                  </Card>
                </Col>
              </Row>
            </TabPanel>
            <TabPanel header="JavaScript">
              <Row>
                <Col xs={4}>
                  <Card>
                    <Card.Header>Nom_projet</Card.Header>
                    <Card.Body>
                      <Card.Title>Description</Card.Title>
                      <Card.Text>Détails...</Card.Text>
                    </Card.Body>
                    <Card.Footer className="text-muted">
                      <Container>
                        <Row>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Container>
                    </Card.Footer>
                  </Card>
                </Col>
                <Col xs={4}>
                  <Card>
                    <Card.Header>Nom_projet</Card.Header>
                    <Card.Body>
                      <Card.Title>Description</Card.Title>
                      <Card.Text>Détails...</Card.Text>
                    </Card.Body>
                    <Card.Footer className="text-muted">
                      <Container>
                        <Row>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={2}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Container>
                    </Card.Footer>
                  </Card>
                </Col>
              </Row>
            </TabPanel>
            <TabPanel header="Deno">
              <Col xs={4}>
                <Card>
                  <Card.Header>Nom_projet</Card.Header>
                  <Card.Body>
                    <Card.Title>Description</Card.Title>
                    <Card.Text>Détails...</Card.Text>
                  </Card.Body>
                  <Card.Footer className="text-muted">
                    <Container>
                      <Row>
                        <Col xs={3} md={2}>
                          <Link to="/user-id">
                            <Image
                              src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                              roundedCircle
                            />
                          </Link>
                        </Col>
                        <Col xs={3} md={2}>
                          <Link to="/user-id">
                            <Image
                              src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                              roundedCircle
                            />
                          </Link>
                        </Col>
                        <Col xs={3} md={2}>
                          <Link to="/user-id">
                            <Image
                              src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                              roundedCircle
                            />
                          </Link>
                        </Col>
                      </Row>
                    </Container>
                  </Card.Footer>
                </Card>
              </Col>
            </TabPanel>
            <TabPanel header="React">
              <Col xs={4}>
                <Card>
                  <Card.Header>Nom_projet</Card.Header>
                  <Card.Body>
                    <Card.Title>Description</Card.Title>
                    <Card.Text>Détails...</Card.Text>
                  </Card.Body>
                  <Card.Footer className="text-muted">
                    <Container>
                      <Row>
                        <Col xs={3} md={2}>
                          <Link to="/user-id">
                            <Image
                              src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                              roundedCircle
                            />
                          </Link>
                        </Col>
                        <Col xs={3} md={2}>
                          <Link to="/user-id">
                            <Image
                              src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                              roundedCircle
                            />
                          </Link>
                        </Col>
                        <Col xs={3} md={2}>
                          <Link to="/user-id">
                            <Image
                              src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                              roundedCircle
                            />
                          </Link>
                        </Col>
                      </Row>
                    </Container>
                  </Card.Footer>
                </Card>
              </Col>
            </TabPanel>
          </TabView>
        </div>

        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>
  );
};
export default project;
