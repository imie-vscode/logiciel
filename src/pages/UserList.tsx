import React from "react";
import LeftMenu from "../components/leftmenu";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import Actions from "../components/actions";
import { Card, Row } from "react-bootstrap";
import Deconnexion from "../components/deconnexion";

const fakeData = [
  {
    name: "fatima elhasbi",
    email: "fatimap@gmail.com",
    date: "13/03/2021",
  },
  {
    name: "adrien M",
    email: "adrien@gmail.com",
    date: "03/03/2021",
  },
];

const UserList = () => {
  return (
    <div className="p-grid">
      <Card id="menu" style={{ height: "100%" }}>
        <div className="p-col-3">
          <br />
          <br />
          <LeftMenu />
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </Card>

      <div className="p-col-9">
        <Row>
          <div className="p-col-11">
            <h3>{`Tous les employés (${fakeData.length})`}</h3>
          </div>
          <div className="p-col-1">
            <Deconnexion></Deconnexion>
          </div>{" "}
        </Row>
        <DataTable
          value={fakeData}
          tableStyle={{ borderRadius: 15 }}
          className="p-datatable-striped"
          selectionMode="single"
        >
          <Column
            field="name"
            header="Nom complet"
            style={{ width: "30%" }}
            filter
            sortable
          />
          <Column
            field="email"
            header="Email"
            style={{ width: "30%" }}
            filter
            sortable
          />
          <Column
            field="date"
            header="Date d'abonnement"
            style={{ width: "30%" }}
            filter
            sortable
          />
          <Column style={{ width: "20%" }} body={() => <Actions />} />
        </DataTable>
      </div>
    </div>
  );
};

export default UserList;
