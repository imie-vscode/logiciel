import React, { useState } from "react";
import { Card } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import { API_BASE_URL } from "../config";
import { useCookies } from "react-cookie";

const Login = () => {
  const history = useHistory();
  const [, setCookie] = useCookies(["user"]);
  const [state, setState] = useState({
    email: "",
    password: "",
  });
  const onChange = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    setState((prev) => ({ ...prev, [name]: value }));
  };
  const onSubmit = async (e: any) => {
    e.preventDefault();
    try {
      const { data } = await axios.post(`${API_BASE_URL}/auth/login`, state);
      setCookie("user", data);
      history.replace("/list-user");
      console.log(data);
    } catch (error) {
      console.log("error: ", error);
    }
  };
  return (
    <form>
      <br />
      <br />
      <br />
      <br />
      <div className="container">
        <div className="row">
          <div className="col-1"></div>

          <div className="col-4">
            <Card>
              <Card.Body>
                <h3 id="text">Se connecter</h3> <br />
                <br />
                <div className="form-group">
                  <label></label>
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    value={state.email}
                    onChange={onChange}
                  />
                </div>
                <div className="form-group">
                  <label></label>
                  <input
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder=" Mot de passe"
                    value={state.password}
                    onChange={onChange}
                  />
                </div>
                <div className="form-group">
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck1"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck1"
                    >
                      Remember me
                    </label>
                  </div>
                  <br />
                </div>
                <Link to="/list-user">
                  <button
                    type="submit"
                    className="btn btn-warning"
                    id="buton"
                    onClick={onSubmit}
                  >
                    Connexion
                  </button>
                </Link>
              </Card.Body>
            </Card>
          </div>

          <div className="col-6" id="imgid">
            <img
              src="https://wallpapercave.com/wp/wp2817089.jpg"
              alt="acc.jpg"
            />
          </div>

          <div className="col-1"></div>
        </div>
      </div>
    </form>
  );
};
export default Login;
