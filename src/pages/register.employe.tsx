import React, { useState } from "react";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { InputTextarea } from "primereact/inputtextarea";
import Deconnexion from "../components/deconnexion";
import { Row } from "react-bootstrap";
import { Button } from "primereact/button";
import { Link } from "react-router-dom";
import { useCookies } from "react-cookie";
import { API_BASE_URL } from "../config";
import axios from "axios";
import { Calendar } from "primereact/calendar";

const NewEmploye = () => {
  const [date2, setDate2] = useState(new Date());

  const [cookie, , removeCookie] = useCookies(["user"]);
  const headers = { authorization: `Bearer ${cookie.user.token}` };
  const [state, setState] = useState({
    firstname: "",
    lastname: "",
    pseudo: "",
    email: "",
    password: "",
    confirm_password: "",
    num_address: "",
    city: "",
    address: "",
    country: "",
    postal_code: "",
    sexe: "",
    birthday: new Date(),
    role: "",
  });
  const onChange = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    setState((prev) => ({ ...prev, [name]: value }));
  };
  const onSubmit = async (e: any) => {
    try {
      const { data } = await axios.post(
        `http://localhost:5000/api/admin/auth/register`,
        state,
        {
          headers,
        }
      );
      console.log(data);
    } catch (error) {
      console.log(error.response.data?.error);
    }

    console.log(state);
  };

  return (
    <div className="container">
      <div className="p-col-12">
        <Row>
          <div className="p-col-1">
            <Link to="/employe-list">
              <i className="pi pi-arrow-left" style={{ fontSize: "2em" }}></i>
            </Link>
          </div>
          <div className="p-col-10">
            <h3>{`Ajouter un employé`}</h3>
          </div>
          <div className="p-col-1">
            <Deconnexion></Deconnexion>
          </div>{" "}
        </Row>
      </div>
      <br />

      <div className="row">
        <div className="col-1"></div>
        <div className="p-fluid p-formgrid p-grid">
          <div className="p-field p-col-12 p-md-6">
            <InputText
              id="firstname6"
              name="firstname"
              type="text"
              placeholder="Nom"
              value={state.firstname}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-12 p-md-6">
            <InputText
              id="lastname6"
              name="lastname"
              type="text"
              placeholder="Prénom"
              value={state.lastname}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-12 p-md-6">
            <InputText
              id="pseudo"
              name="pseudo"
              type="text"
              placeholder="Pseudo"
              value={state.pseudo}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-6">
            <InputText
              name="email"
              type="mail"
              placeholder="Adresse mail"
              value={state.email}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-6">
            <InputText
              name="password"
              type="password"
              placeholder="Mot de passe"
              value={state.password}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-6">
            <InputText
              name="confirm_password"
              type="password"
              placeholder="confirmer le mot de passe"
              value={state.confirm_password}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-1">
            <InputText
              name="num_address"
              type="text"
              placeholder="num_address"
              value={state.num_address}
              onChange={onChange}
            />
          </div>

          <div className="p-field p-col-9">
            <InputText
              name="address"
              type="text"
              placeholder="Adresse"
              value={state.address}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-2">
            <InputText
              name="postal_code"
              type="number"
              placeholder="code postal"
              value={state.postal_code}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-12 p-md-2">
            <InputText
              name="country"
              type="text"
              placeholder="pays"
              value={state.country}
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-12 p-md-3">
            <Dropdown
              value={state.sexe}
              //   options={cities}
              optionLabel="sexe"
              placeholder="Sexe"
              onChange={onChange}
            />
          </div>

          <div className="p-field p-col-12 p-md-3">
            <Dropdown
              value={state.role}
              //   options={cities}
              optionLabel="Rôle"
              placeholder="Rôle"
              onChange={onChange}
            />
          </div>
          <div className="p-field p-col-12 p-md-4">
            <Calendar
              id="icon"
              //   value={date2}
              //   onChange={(e) => setDate2(e.value)}
              showIcon
              value={state.birthday}
              onChange={onChange}
            />
          </div>
        </div>
        <div className="col-1"></div>
      </div>
      <div className="p-field p-col-12 p-md-@">
        <span className="p-buttonset">
          <Button label="Enregistrer" icon="pi pi-check" onClick={onSubmit} />
          <Button label="Supprimer" icon="pi pi-trash" />
        </span>
      </div>
    </div>
  );
};

export default NewEmploye;
