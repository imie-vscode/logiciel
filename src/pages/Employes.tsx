import Card from "react-bootstrap/Card";
import LeftMenu from "../components/leftmenu";
import "primeflex/primeflex.css";
import Actions from "../components/actions";
import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { Link } from "react-router-dom";
import Deconnexion from "../components/deconnexion";
import React from "react";
import { Row } from "react-bootstrap";
import Actions2 from "../components/Actions2";

const fakeData = [
  {
    name: "fatima elhasbi",
    email: "fatimap@gmail.com",
    date: "13/03/2021",
  },
  {
    name: "adrien M",
    email: "adrien@gmail.com",
    date: "03/03/2021",
  },
];

const EmployesList = () => {
  // function handleClick() {
  //     alert('Le lien a été cliqué.');
  //   }
  return (
    <div className="p-grid">
      <Card id="menu">
        <div className="p-col-3">
          <div className="p-col-2" id="menu">
            <Link to="/list-user">
              <i className="pi pi-arrow-left" style={{ fontSize: "2em" }}></i>
            </Link>{" "}
          </div>
          <LeftMenu />
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </Card>
      <div className="p-col-9">
        <Row>
          <div className="p-col-11">
            <h3>{`Tous les Employés (${fakeData.length})`}</h3>
          </div>
          <div className="p-col-1">
            <Deconnexion></Deconnexion>
          </div>{" "}
        </Row>

        <DataTable
          value={fakeData}
          tableStyle={{ borderRadius: 15 }}
          className="p-datatable-striped"
          selectionMode="single"
        >
          <Column
            field="name"
            header="Nom complet"
            style={{ width: "30%" }}
            filter
            sortable
          />
          <Column
            field="email"
            header="Email"
            style={{ width: "30%" }}
            filter
            sortable
          />
          <Column
            field="date"
            header="Date d'abonnement"
            style={{ width: "30%" }}
            filter
            sortable
          />
          <Column style={{ width: "5%" }} body={() => <Actions />} />
          <Column style={{ width: "10%" }} body={() => <Actions2></Actions2>} />
        </DataTable>
      </div>{" "}
    </div>
  );
};
export default EmployesList;
