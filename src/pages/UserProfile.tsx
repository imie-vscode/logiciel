import { Link } from "react-router-dom";
import { Col, Container, Row } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { Table, Image } from "react-bootstrap";
import React from "react";
import { Button } from "primereact/button";
import Deconnexion from "../components/deconnexion";
import { Fieldset } from "primereact/fieldset";

const UserProfil = () => {
  // function handleClick() {
  //     alert('Le lien a été cliqué.');
  //   }
  return (
    <div className="p-grid">
      <div className="p-col-3" id="menu">
        <Card id="menu">
          <Container>
            <div className="p-col-2" id="menu">
              <Link to="/list-user">
                <i className="pi pi-arrow-left" style={{ fontSize: "2em" }}></i>
              </Link>{" "}
            </div>
            <div className="p-col-10" id="menu"></div>
            <Card.Header>
              <Row>
                <Col xs={4} md={3}></Col>
                <Col xs={4} md={7}>
                  <Link to="/user-id">
                    <Image
                      id="white"
                      src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                      roundedCircle
                    />
                  </Link>
                </Col>
                <Col xs={4} md={3}></Col>
              </Row>
            </Card.Header>
            <Card.Body id="menu" style={{ color: "white" }}>
              <Card.Title>Nom et Prenom</Card.Title>
              <Card.Text>adressemail@gmail.com</Card.Text>{" "}
              <Card.Text>Sexe</Card.Text>{" "}
              <Card.Text>Membre depuis: dd/mm/yyyy</Card.Text>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <Button
                label="Résillier l'abonnement"
                className="p-button-secondary p-button-rounded"
              />
              <br />
              <br />
              <Button
                label="  Supprimer le compte "
                className="p-button-secondary p-button-rounded"
              />
              <br />
              <br />
              <br />
              <br />
            </Card.Body>
          </Container>
        </Card>
      </div>

      <div className="p-col-9">
        <Card.Header>
          <div className="p-grid">
            <div className="p-col-11"></div>
            <div className="p-col-1">
              <Deconnexion></Deconnexion>
            </div>
          </div>
        </Card.Header>
        <div className="p-col-9">
          <div>
            <div className="card">
              <Fieldset legend="Facture">
                <Table striped bordered hover size="sm">
                  <thead>
                    <tr>
                      <th>type d'abonnement </th>
                      <th>N° de facture</th>
                      <th>Date de paiement</th>
                      <th>Montant</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Premium</td>
                      <td>749372</td>
                      <td>13/03/2021</td>
                      <td>9,99 </td>
                    </tr>
                    <tr>
                      <td>
                        {" "}
                        <br></br>
                      </td>
                      <td> </td>
                      <td> </td>
                      <td> </td>
                    </tr>
                  </tbody>
                </Table>
              </Fieldset>
              <Fieldset legend="Projets">
                <Row>
                  <Col xs={4}>
                    <Card>
                      <Card.Header>Nom_projet</Card.Header>
                      <Card.Body>
                        <Card.Title>Description</Card.Title>
                        <Card.Text>Détails...</Card.Text>
                      </Card.Body>
                      <Card.Footer className="text-muted">
                        <Row>
                          <Col xs={3} md={3}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={3}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={3}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Card.Footer>
                    </Card>
                  </Col>
                  <Col xs={4}>
                    <Card>
                      <Card.Header>Nom_projet</Card.Header>
                      <Card.Body>
                        <Card.Title>Description</Card.Title>
                        <Card.Text>Détails...</Card.Text>
                      </Card.Body>
                      <Card.Footer className="text-muted">
                        <Row>
                          <Col xs={3} md={3}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={3}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                          <Col xs={3} md={3}>
                            <Link to="/user-id">
                              <Image
                                src="https://cdn.icon-icons.com/icons2/2442/PNG/512/profile_user_icon_148618.png"
                                roundedCircle
                              />
                            </Link>
                          </Col>
                        </Row>
                      </Card.Footer>
                    </Card>
                  </Col>
                </Row>
              </Fieldset>
            </div>
          </div>
        </div>
        <div className="p-col-3"></div>
      </div>
    </div>
  );
};
export default UserProfil;
