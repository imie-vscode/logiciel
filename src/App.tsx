import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import {
  HashRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import Login from "./pages/login.component";
import UserList from "./pages/UserList";
import project from "./pages/Project";
import EmployesList from "./pages/Employes";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import UserProfil from "./pages/UserProfile";
import GestionRole from "./pages/GestionRole";
import NewEmploye from "./pages/register.employe";
import { useCookies } from "react-cookie";
import history from "./history";

function App() {
  const [cookie] = useCookies(["user"]);
  const AdminRoute = ({ ...props }) => {
    return cookie.user ? <Route {...props} /> : <Redirect to="/" />;
  };
  return (
    <Router basename="/">
      <div className="App" id="back">
        <div className="container">
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-in"}>
                  Login
                </Link>
              </li>
            </ul>
          </div>
        </div>

        <div className="auth-wrapper" id="back">
          <div className="auth-inner">
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/sign-in" component={Login} />
              <AdminRoute exact path="/list-user" component={UserList} />
              <AdminRoute exact path="/project" component={project} />
              <AdminRoute exact path="/employe-list" component={EmployesList} />
              <AdminRoute exact path="/user-id" component={UserProfil} />
              <AdminRoute exact path="/role" component={GestionRole} />
              <AdminRoute path="/new-employe" component={NewEmploye} />
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
